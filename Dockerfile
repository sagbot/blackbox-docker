ARG FLUENTD_VERSION=1.9.1
ARG LINUX_DISTRO=debian

FROM fluent/fluentd:v${FLUENTD_VERSION}-${LINUX_DISTRO}-1.0

USER root

RUN fluent-gem install fluent-plugin-elasticsearch

RUN apt-get update -y && \
    apt-get install -y curl=7.64.0-4+deb10u2 \
                       iputils-ping=3:20180629-2+deb10u2 \
                       bc=1.07.1-2+b1 \
                       jq=1.5+dfsg-2+b1 \
                       curl=7.64.0-4+deb10u2

USER fluent
